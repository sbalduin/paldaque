# PalDaQue - PalaestrAI Database Query

This is a small command line tool to get most out of your palaestrAI experiment store.

## Installation

If you're new to Python and/or palaestrAI, have a look at the [Classic ARL Playground](https://gitlab.com/arl2/classic-arl-playground), which contains basic instructions to get a working version of [palaestrAI](https://docs.palaestr.ai/) and a virtual environment.

Currently, there is no [PyPi](https://pypi.org/) package, so please install it with

```bash
pip install git+https://gitlab.com/sbalduin/paldaque.git
```

## Usage

So, you have conducted an experiment (or more) with palaestrAI but don't know how to access the results?
Here we go!

First, lets get an overview of all the experiments in the database:

```bash
paldaque experiment
```

As you might know, each experiment can have one or more runs. 
Lets get all the runs in the database:

```bash
paldaque experiment-run
```

If you already know that you want to look only at the runs of a specific experiment (and you know the experiment ID from the previous command, say it is `1`):

```bash
paldaque experiment-run -e 1
```

Okay, now we know the experiment run ID (say `1`), then lets have a look at all the instances:

```bash
paldaque experiment-run-instance -r 1
```

This will give you all the repeated executions of that run (which is the definition of a run instance btw).
We will select one instance (say `1` for the sake of "tradition") and have a look at all the phases:

```bash
paldaque experiment-run-phase -i 1
```

You may recognize the pattern: now we have all the phase IDs of our instance of interest and select one of them (you guess which one, right?).
Now it will get interesting since we will look at the actual results.
Hrr hrr hrr.

```bash
paldaque muscle-action -p 1
```

Okay. 
But why are there no sensor readings and so on?
Try it out and you know why:

```bash
paldaque muscle-action -p 1 -f
```

Will give you the full output.
My terminal is not wide enough to display everything, but may yours is.

For everyone else, you can export the results to csv:

```bash
paldaque muscle-action -p 1 --csv results.csv
```

Now you have them in a csv file. 
Yay!

## Tweaks

Using the `--help` command will give you hints on all possible options for the commands, e.g.,

```bash
paldaque --help
paldaque muscle-action --help
```

Maybe you do not only want the results of a single phase, but of an instance/run/experiment?
You can provide the corresponding IDs with `-i` for instance, `-r` for run, and `-e` for experiment.
Providing a "lower" level ID will ignore higher ones (because a phase can only be part of one instance can only be part of one run can only be part of one experiment).

You have a different database running somewhere?
You can change the database by providing the `--store-uri` (or short `-s`) option:

```bash
paldaque -s "sqlite:///palaestrai.db" muscle-action
```

Note: there will be no check if the database exists.
You may get errors it that is not the case!

## API

Yes, there is an API so you will be able to integrate `paldaque` directly into your application.

The equivalent code for `paldaque -s "sqlite:///palaestrai.db" muscle-action -p 1 -f` would be something like this:

```python
from tabulate import tabulate

from paldaque.api_muscle_action import read_muscle_actions
from palaestrai.core import RuntimeConfig

# If you want to use a custom store uri
RuntimeConfig().load({"store_uri": "sqlite:///palaestraid.db"})

results = read_muscle_actions(
    experiment_id=0,
    experiment_run_id=0,
    experiment_run_instance_id=0,
    experiment_run_phase_id=1,
    as_dict=True
)

print(tabulate(results, headers="keys", tablefmt="pipe"))
```

If you change `as_dict` to `False`, the `results` object will be a list containing dictionaries for each row.
Otherwise, it will be a dictionary with lists for each column.
